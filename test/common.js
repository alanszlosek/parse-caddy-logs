var tap = require('tap');

var parse_caddy_logs = require('../index');

/*

*/
/*

1.22.333.123 - [14/Feb/2018:14:00:03 +0000] "POST /endpoint HTTP/1.1" 200 1431 "-" "Dalvik/2.1.0 (Linux; U; Android 6.0; MotoG4)"
123.33.222.45 - [14/Feb/2018:14:00:04 +0000] "GET /index.php HTTP/1.1" 200 30 "-" "curl/7.38.0"
*/

// Caddy added {user} placeholder to log format in March 2017
// https://caddy.community/t/custom-user-placeholder-along-with-header-directive/1676
// Ensure we can parse the old format
tap.test('common.nouser', function(test) {
  var lines = [
    '1.22.333.123 - [06/Jan/2017:13:40:00 +0000] "GET /js/app.js HTTP/1.0" 200 30',
    '1.22.333.123 - [06/Jan/2017:13:40:01 +0000] "GET /js/connect.js HTTP/1.1" 404 300',
    '1.22.333.123 - [06/Jan/2017:13:40:02 +0000] "GET / HTTP/2.0" 200 53535',
    '1.22.333.123 - [14/Feb/2018:14:00:03 +0000] "POST /endpoint HTTP/1.1" 200 1431',
    '123.33.222.45 - [14/Feb/2018:14:00:04 +0000] "GET /index.php HTTP/1.1" 304 0'
  ];
  var obj = [
    {
      remote_address: '1.22.333.123',
      basicauth_user: '',
      datetime: '06/Jan/2017:13:40:00 +0000',
      http_method: 'GET',
      request_uri: '/js/app.js',
      http_version: '1.0',
      http_status: '200',
      bytes_sent: '30',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: '',
      datetime: '06/Jan/2017:13:40:01 +0000',
      http_method: 'GET',
      request_uri: '/js/connect.js',
      http_version: '1.1',
      http_status: '404',
      bytes_sent: '300',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: '',
      datetime: '06/Jan/2017:13:40:02 +0000',
      http_method: 'GET',
      request_uri: '/',
      http_version: '2.0',
      http_status: '200',
      bytes_sent: '53535',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: '',
      datetime: '14/Feb/2018:14:00:03 +0000',
      http_method: 'POST',
      request_uri: '/endpoint',
      http_version: '1.1',
      http_status: '200',
      bytes_sent: '1431',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '123.33.222.45',
      basicauth_user: '',
      datetime: '14/Feb/2018:14:00:04 +0000',
      http_method: 'GET',
      request_uri: '/index.php',
      http_version: '1.1',
      http_status: '304',
      bytes_sent: '0',
      referer: '',
      user_agent: ''
    }
  ];

  for (var i = 0; i < lines.length; i++) {
    test.same(
      parse_caddy_logs.access( lines[i] ),
      obj[i]
    );
  }
  test.done();
});

// https://caddy.community/t/custom-user-placeholder-along-with-header-directive/1676
tap.test('common.withuser', function(test) {
  var lines = [
    '1.22.333.123 - - [06/Jan/2017:13:40:00 +0000] "GET /js/app.js HTTP/1.0" 200 30',
    '1.22.333.123 - - [06/Jan/2017:13:40:01 +0000] "GET /js/connect.js HTTP/1.1" 404 300',
    '1.22.333.123 - authuser [06/Jan/2017:13:40:02 +0000] "GET / HTTP/2.0" 200 53535',
    '1.22.333.123 - - [14/Feb/2018:14:00:03 +0000] "POST /endpoint HTTP/1.1" 200 1431',
    '123.33.222.45 - - [14/Feb/2018:14:00:04 +0000] "GET /index.php HTTP/1.1" 304 0'
  ];
  var obj = [
    {
      remote_address: '1.22.333.123',
      basicauth_user: '-',
      datetime: '06/Jan/2017:13:40:00 +0000',
      http_method: 'GET',
      request_uri: '/js/app.js',
      http_version: '1.0',
      http_status: '200',
      bytes_sent: '30',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: '-',
      datetime: '06/Jan/2017:13:40:01 +0000',
      http_method: 'GET',
      request_uri: '/js/connect.js',
      http_version: '1.1',
      http_status: '404',
      bytes_sent: '300',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: 'authuser',
      datetime: '06/Jan/2017:13:40:02 +0000',
      http_method: 'GET',
      request_uri: '/',
      http_version: '2.0',
      http_status: '200',
      bytes_sent: '53535',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '1.22.333.123',
      basicauth_user: '-',
      datetime: '14/Feb/2018:14:00:03 +0000',
      http_method: 'POST',
      request_uri: '/endpoint',
      http_version: '1.1',
      http_status: '200',
      bytes_sent: '1431',
      referer: '',
      user_agent: ''
    },
    {
      remote_address: '123.33.222.45',
      basicauth_user: '-',
      datetime: '14/Feb/2018:14:00:04 +0000',
      http_method: 'GET',
      request_uri: '/index.php',
      http_version: '1.1',
      http_status: '304',
      bytes_sent: '0',
      referer: '',
      user_agent: ''
    }
  ];

  for (var i = 0; i < lines.length; i++) {
    test.same(
      parse_caddy_logs.access( lines[i] ),
      obj[i]
    );
  }
  test.done();
});
