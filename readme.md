Parse Caddy Logs
====

This is a simple Caddy server log parser. Currently it parses HTTP access logs to JSON format.

Install with npm:

    npm install parse-caddy-logs


Example Usage
====

	var parse_caddy_logs = require('parse-caddy-logs');

	// Get your access log line from somewhere
	var line = 'bla';
	var obj = parse_caddy_logs.access( line );


Example Output
====

	{
		remote_address: '1.22.333.123',
		datetime: '06/Jan/2017:13:40:00 +0000',
		http_method: 'GET',
		request_uri: '/js/app.js',
		http_version: '1.0',
		http_status: '200',
		bytes_sent: '30',
		referer: '',
		user_agent: ''
	}


TESTS
====

Run `npm test` to run unit tests.

LICENSE
====

MIT License. See LICENSE file for more information.

