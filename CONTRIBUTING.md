Contributions welcome!

Simply fork the repo, make your changes, and send me a pull request. If you don't know how to do those things, email me and I'll walk you through it: alan.szlosek@gmail.com