module.exports = {

  // Parses Caddy default log format:
  // {remote} - {user} [{when}] \"{method} {uri} {proto}\" {status} {size} \"{>Referer}\" \"{>User-Agent}\"
  access: function(input) {
    var pattern = /^(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}) -( (\S+))? \[([^\]]+)\] \"(\S+) (\S+) ((\w+)\/(\d+\.\d))\" (\d{3}) (\d+)( \"(\S+)\" \"([^"]+)\")?$/;
    var match = input.match(pattern);
    if (!match) {
      return null;
    }
    return {
      remote_address: match[1],
      basicauth_user: match[3] || '',
      datetime: match[4],
      http_method: match[5],
      request_uri: match[6],
      http_version: match[9],
      http_status: match[10],
      bytes_sent: match[11],
      referer: match[13] || '',
      user_agent: match[14] || ''
    };
  }
}
